﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XWPF.UserModel;
//using NPOI.OpenXml4Net.OPC;
using System.IO;
using System.Diagnostics;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Grade
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class Student
        {
            public string EnglishName { get; set; }
            public string ChineseName { get; set; }
            public string EnglishGroup { get; set; }
            public string Homeroom { get; set; }
            public string Phonics { get; set; }
            public string Listening { get; set; }
            public string Speaking { get; set; }
            public string Reading { get; set; }
            public string Grammar { get; set; }
            public string Grade { get; set; }

            public override string ToString()
            {
                return (
                    EnglishName + ','
                    + ChineseName + ','
                    + EnglishGroup + ','
                    + Homeroom + ','
                    + Phonics + ','
                    + Listening + ','
                    + Speaking + ','
                    + Reading + ','
                    + Grammar + '.');
            }
        }
        #region Global Var
        List<Student> students = new List<Student>();
        string[] prop_grade = new string[] { "Phonics", "Listening", "Speaking", "Reading", "Grammar", "Grade" };
        string[] grade_index = new string[] { "C-", "C", "C+", "B-", "B", "B+", "A-", "A", "A+" };
        XWPFDocument doc;
        XWPFDocument doc_template;
        //OPCPackage doc_template;
        string f = "template_docx.docx";
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private bool Read_excel()
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Excel Files(*.xlsx)|*.xlsx|Excel Files(*.xls)|*.xls";
                //dialog.InitialDirectory = @".\";

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    IWorkbook workBook;
                    using (var fs = new FileStream(dialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        if (Path.GetExtension(dialog.FileName).ToLower() == ".xls")
                        { workBook = new HSSFWorkbook(fs); }
                        else { workBook = new XSSFWorkbook(fs); }
                    }
                    var sheet = workBook.GetSheetAt(0);
                    IRow rowHeader = sheet.GetRow(0);
                    students.Clear();
                    for (int i = 1; i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        var student = new Student();
                        for (int j = 0; j < row.LastCellNum; j++) //每一列 
                        {
                            string cellValue = row.GetCell(j).ToString();
                            var property = student.GetType().GetProperty(rowHeader.GetCell(j).ToString());//reflection
                            property.SetValue(student, sheet.GetRow(i).GetCell(j)?.ToString());//set value
                        }
                        students.Add(student);
                    }
                    workBook.Close();
                    Form_Refresh();
                    bindingSource1.DataSource = students;
                    bindingSource1.Position = 0;
                    return true;
                }
                else
                {
                    //Exit if not choose correct Excel File
                    //Close();
                    //Environment.Exit(1);
                    return false;
                }
            }
            catch { throw; }
        }

        private void Load_word_template()
        {
            using FileStream stream = File.OpenRead(f);
            doc_template = new XWPFDocument(stream);
            //doc_template = OPCPackage.Open(stream);
        }

        private void Write_word(Student stu, string fpath)
        {
            //測試用預設檔案名稱
            string filename = Path.Combine(fpath, stu.EnglishName + '-' + DateTime.Today.ToString("yyyy-MM-dd") + ".docx");
            FileStream output = new FileStream(filename, FileMode.Create);
            doc.Write(output);
            output.Close();
            output.Dispose();
        }

        private void Replace_text(Student stu)
        {
            var tables = doc.Tables;
            foreach (var table in tables)
            {
                foreach (var row in table.Rows)
                {
                    foreach (var cell in row.GetTableCells())
                    {
                        foreach (var para in cell.Paragraphs)
                        {
                            if (para.Runs.Any())
                            {
                                string[] props = { "EnglishName", "ChineseName", "EnglishGroup", "Homeroom" };
                                foreach (string prop in props)
                                {
                                    if (para.ParagraphText.Contains("${" + prop + "}"))
                                    {
                                        //replace text with object value
                                        string new_string = para.ParagraphText.Replace("${" + prop + "}", (string)stu.GetType().GetProperty(prop).GetValue(stu, null));
                                        while (int.Parse(para.Runs.Count.ToString()) > 1) { para.RemoveRun(0); }
                                        para.Runs[0].SetText(new_string);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void Fill_grade(Student stu)
        {
            var tables = doc.Tables;
            foreach (var table in tables)
            {
                if (table.Text.Contains("Language Skills Performance"))
                {
                    foreach (var row in table.Rows)
                    {
                        //第一個column
                        string caseswitch = row.GetCell(0).Paragraphs[0].Text;
                        switch (caseswitch)
                        {
                            case "Phonics":
                            case "Listening":
                            case "Speaking":
                            case "Reading":
                            case "Grammar and language usage":

                                //Object has prop different name
                                if (caseswitch == "Grammar and language usage") caseswitch = "Grammar";

                                //index of checked by grade
                                int i = 6 - int.Parse((string)stu.GetType().GetProperty(caseswitch).GetValue(stu, null));

                                //should be clear
                                var para = row.GetCell(i).Paragraphs[0];
                                XWPFRun run = para.CreateRun();
                                run.SetText("\u2611", 0);
                                break;

                            case "Grade":
                                para = row.GetCell(1).Paragraphs[0];
                                while (int.Parse(para.Runs.Count.ToString()) > 1) { para.RemoveRun(0); }
                                para.Runs[0].SetText(stu.Grade.ToString());
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        private void Form_Refresh()
        {
            foreach (Control item in Controls)
            {
                //Enable all button
                if (item is Button) item.Enabled = true;
            }
        }

        private void Grade_Button(object sender, EventArgs e)
        {
            Button but = (Button)sender;
            string[] key;
            key = but.Name.Split(new string[] { "button" }, StringSplitOptions.None);

            //Grade decode
            if (key[0] == "Grade")
            {
                key[1] = grade_index[int.Parse(key[1]) - 1];
            }

            Student stu = ((Student)bindingSource1.Current);
            stu.GetType().GetProperty(key[0]).SetValue(stu, key[1]);
            //Enable all other options
            foreach (Control item in this.Controls)
            {
                if (item.Name.Contains(key[0]) && item is Button) { item.Enabled = true; }
            }
            but.Enabled = false;
        }

        private void Done_Click(object sender, EventArgs e)
        {
            //Next student
            if (bindingSource1.Position + 1 <= bindingSource1.Count) { bindingSource1.Position++; }
        }

        private void Read_Student()
        {
            Student stu = (Student)bindingSource1.Current;
            label_StuEnName.Text = stu.EnglishName;
            label_StuEnGroup.Text = stu.EnglishGroup;
            label_StuName.Text = stu.ChineseName;
            label_StuRoom.Text = stu.Homeroom;

            //Check if already have grade
            List<string> list_k = new List<string>();
            List<string> list_v = new List<string>();
            foreach (string prop in prop_grade)
            {
                if (stu.GetType().GetProperty(prop).GetValue(stu) != null)
                {
                    list_k.Add(prop);
                    if (prop == "Grade")
                    {
                        string temp = stu.GetType().GetProperty(prop).GetValue(stu).ToString();
                        temp = (Array.IndexOf(grade_index, temp) + 1).ToString();
                        list_v.Add(temp);
                    }
                    list_v.Add(stu.GetType().GetProperty(prop).GetValue(stu).ToString());
                }
            }

            //Make that grade button disabled
            if (list_k.Count > 0)
            {
                //foreach (Control item in this.Controls)
                for (int i = 0; i < Controls.Count; i++)
                {
                    Control item = Controls[i];
                    if (item is Button)
                    {
                        for (int j = 0; j < list_k.Count; j++)
                        {
                            // Choose the button of specific grade
                            if (item.Name.Contains(list_k[j]) && item.Name.Contains(list_v[j]))
                            {
                                Controls[i].Enabled = false;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void BindingNavigatorPositionItem_TextChanged(object sender, EventArgs e)
        {
            //確認有資料,Form關閉會先清空
            if (bindingSource1.Count >= 1)
            {
                Form_Refresh();
                Read_Student();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //提前釋放資源
            bindingSource1.Clear();
            bindingSource1.Dispose();
            bindingNavigator1.Dispose();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (students.Count > 0) { Save_Reports(); }
        }

        private void Save_Reports()
        {
            foreach (Student stu in students)
            {
                //Error
                if (!Check_grade_valid(stu))
                {
                    bindingSource1.Position = students.IndexOf(stu);
                    return;
                }
            }
            try
            {
                CommonOpenFileDialog dialog = new CommonOpenFileDialog();
                //dialog.InitialDirectory = Directory.GetCurrentDirectory();
                dialog.IsFolderPicker = true;
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    foreach (Student stu in students)
                    {
                        Load_word_template();
                        doc = doc_template;
                        //doc = new XWPFDocument(doc_template);
                        Replace_text(stu);
                        Fill_grade(stu);
                        Write_word(stu, dialog.FileName);
                    }
                }
                else {/*Nothing*/}
            }
            catch (Exception) { throw; }
        }

        private bool Check_grade_valid(Student stu)
        {
            bool error = false;
            string error_mes = "";

            //Check grade
            foreach (string prop in prop_grade)
            {
                //missing grade
                if (stu.GetType().GetProperty(prop).GetValue(stu) == null)
                {
                    error = true;
                    error_mes += " " + prop + " ";
                }
            }

            //Everything correct
            if (!error) { return true; }

            //doesn't choose all grade
            else { MessageBox.Show(stu.EnglishName + "'s Grade is not Complete!\n" + error_mes); return false; }
        }

        private void Button_Test_Click(object sender, EventArgs e)
        {
            Random ran = new Random();
            string i = ran.Next(1, 6).ToString();
            foreach (Control item in Controls)
            {
                if (item is Button && item.Name.Contains(i))
                {
                    Grade_Button(item, EventArgs.Empty);
                }
            }
        }

        private void Read_excel_click(object sender, EventArgs e)
        {
            if (Read_excel())
            {
                toolStripButton_Save.Enabled = true;
                button_Done.Enabled = true;

                #if DEBUG
                button_Test.Enabled = true;
                #endif
            }

        }

        private void Button_load_Template_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Word Files(*.docx)|*.docx";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    f = dialog.FileName;
                    //Load_word_template(dialog.FileName);
                }
                else {/*Nothing*/ }
            }
            catch (Exception) { throw; }
        }
    }
}
