﻿namespace Grade
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label_StuEnName = new System.Windows.Forms.Label();
            this.label_StuEnGroup = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_StuName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_StuRoom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Phonicsbutton5 = new System.Windows.Forms.Button();
            this.Phonicsbutton4 = new System.Windows.Forms.Button();
            this.Phonicsbutton3 = new System.Windows.Forms.Button();
            this.Phonicsbutton2 = new System.Windows.Forms.Button();
            this.Phonicsbutton1 = new System.Windows.Forms.Button();
            this.Listeningbutton5 = new System.Windows.Forms.Button();
            this.Listeningbutton4 = new System.Windows.Forms.Button();
            this.Listeningbutton3 = new System.Windows.Forms.Button();
            this.Listeningbutton2 = new System.Windows.Forms.Button();
            this.Listeningbutton1 = new System.Windows.Forms.Button();
            this.Speakingbutton5 = new System.Windows.Forms.Button();
            this.Speakingbutton4 = new System.Windows.Forms.Button();
            this.Speakingbutton3 = new System.Windows.Forms.Button();
            this.Speakingbutton2 = new System.Windows.Forms.Button();
            this.Speakingbutton1 = new System.Windows.Forms.Button();
            this.Readingbutton5 = new System.Windows.Forms.Button();
            this.Readingbutton4 = new System.Windows.Forms.Button();
            this.Readingbutton3 = new System.Windows.Forms.Button();
            this.Readingbutton2 = new System.Windows.Forms.Button();
            this.Readingbutton1 = new System.Windows.Forms.Button();
            this.Grammarbutton5 = new System.Windows.Forms.Button();
            this.Grammarbutton4 = new System.Windows.Forms.Button();
            this.Grammarbutton3 = new System.Windows.Forms.Button();
            this.Grammarbutton2 = new System.Windows.Forms.Button();
            this.Grammarbutton1 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.Gradebutton9 = new System.Windows.Forms.Button();
            this.Gradebutton8 = new System.Windows.Forms.Button();
            this.Gradebutton7 = new System.Windows.Forms.Button();
            this.Gradebutton6 = new System.Windows.Forms.Button();
            this.Gradebutton5 = new System.Windows.Forms.Button();
            this.Gradebutton4 = new System.Windows.Forms.Button();
            this.Gradebutton3 = new System.Windows.Forms.Button();
            this.Gradebutton2 = new System.Windows.Forms.Button();
            this.Gradebutton1 = new System.Windows.Forms.Button();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton_Read = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_load_Template = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
#if DEBUG
            this.button_Test = new System.Windows.Forms.Button();
#endif
            this.button_Done = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "English Name";
            // 
            // label_StuEnName
            // 
            this.label_StuEnName.AutoSize = true;
            this.label_StuEnName.Location = new System.Drawing.Point(138, 42);
            this.label_StuEnName.Name = "label_StuEnName";
            this.label_StuEnName.Size = new System.Drawing.Size(23, 16);
            this.label_StuEnName.TabIndex = 1;
            this.label_StuEnName.Text = "***";
            // 
            // label_StuEnGroup
            // 
            this.label_StuEnGroup.AutoSize = true;
            this.label_StuEnGroup.Location = new System.Drawing.Point(138, 71);
            this.label_StuEnGroup.Name = "label_StuEnGroup";
            this.label_StuEnGroup.Size = new System.Drawing.Size(23, 16);
            this.label_StuEnGroup.TabIndex = 3;
            this.label_StuEnGroup.Text = "***";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "English Group";
            // 
            // label_StuName
            // 
            this.label_StuName.AutoSize = true;
            this.label_StuName.Location = new System.Drawing.Point(138, 104);
            this.label_StuName.Name = "label_StuName";
            this.label_StuName.Size = new System.Drawing.Size(23, 16);
            this.label_StuName.TabIndex = 5;
            this.label_StuName.Text = "***";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 16);
            this.label6.TabIndex = 4;
            this.label6.Text = "Chinese Name";
            // 
            // label_StuRoom
            // 
            this.label_StuRoom.AutoSize = true;
            this.label_StuRoom.Location = new System.Drawing.Point(138, 135);
            this.label_StuRoom.Name = "label_StuRoom";
            this.label_StuRoom.Size = new System.Drawing.Size(23, 16);
            this.label_StuRoom.TabIndex = 7;
            this.label_StuRoom.Text = "***";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 6;
            this.label8.Text = "Homeroom";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 207);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 16);
            this.label9.TabIndex = 8;
            this.label9.Text = "Phonics";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 247);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 16);
            this.label10.TabIndex = 9;
            this.label10.Text = "Listening";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(50, 287);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "Speaking";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(50, 327);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 11;
            this.label12.Text = "Reading";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(50, 367);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 32);
            this.label13.TabIndex = 12;
            this.label13.Text = "Grammar and \r\nlanguage usage";
            // 
            // Phonicsbutton5
            // 
            this.Phonicsbutton5.Enabled = false;
            this.Phonicsbutton5.Location = new System.Drawing.Point(165, 204);
            this.Phonicsbutton5.Name = "Phonicsbutton5";
            this.Phonicsbutton5.Size = new System.Drawing.Size(75, 23);
            this.Phonicsbutton5.TabIndex = 13;
            this.Phonicsbutton5.Text = "Excellent";
            this.Phonicsbutton5.UseVisualStyleBackColor = true;
            this.Phonicsbutton5.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Phonicsbutton4
            // 
            this.Phonicsbutton4.Enabled = false;
            this.Phonicsbutton4.Location = new System.Drawing.Point(257, 204);
            this.Phonicsbutton4.Name = "Phonicsbutton4";
            this.Phonicsbutton4.Size = new System.Drawing.Size(75, 23);
            this.Phonicsbutton4.TabIndex = 14;
            this.Phonicsbutton4.Text = "great";
            this.Phonicsbutton4.UseVisualStyleBackColor = true;
            this.Phonicsbutton4.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Phonicsbutton3
            // 
            this.Phonicsbutton3.Enabled = false;
            this.Phonicsbutton3.Location = new System.Drawing.Point(349, 204);
            this.Phonicsbutton3.Name = "Phonicsbutton3";
            this.Phonicsbutton3.Size = new System.Drawing.Size(75, 23);
            this.Phonicsbutton3.TabIndex = 15;
            this.Phonicsbutton3.Text = "good";
            this.Phonicsbutton3.UseVisualStyleBackColor = true;
            this.Phonicsbutton3.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Phonicsbutton2
            // 
            this.Phonicsbutton2.Enabled = false;
            this.Phonicsbutton2.Location = new System.Drawing.Point(441, 204);
            this.Phonicsbutton2.Name = "Phonicsbutton2";
            this.Phonicsbutton2.Size = new System.Drawing.Size(75, 23);
            this.Phonicsbutton2.TabIndex = 16;
            this.Phonicsbutton2.Text = "fair";
            this.Phonicsbutton2.UseVisualStyleBackColor = true;
            this.Phonicsbutton2.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Phonicsbutton1
            // 
            this.Phonicsbutton1.Enabled = false;
            this.Phonicsbutton1.Location = new System.Drawing.Point(533, 204);
            this.Phonicsbutton1.Name = "Phonicsbutton1";
            this.Phonicsbutton1.Size = new System.Drawing.Size(109, 23);
            this.Phonicsbutton1.TabIndex = 17;
            this.Phonicsbutton1.Text = "Need Improve";
            this.Phonicsbutton1.UseVisualStyleBackColor = true;
            this.Phonicsbutton1.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Listeningbutton5
            // 
            this.Listeningbutton5.Enabled = false;
            this.Listeningbutton5.Location = new System.Drawing.Point(165, 244);
            this.Listeningbutton5.Name = "Listeningbutton5";
            this.Listeningbutton5.Size = new System.Drawing.Size(75, 23);
            this.Listeningbutton5.TabIndex = 22;
            this.Listeningbutton5.Text = "Excellent";
            this.Listeningbutton5.UseVisualStyleBackColor = true;
            this.Listeningbutton5.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Listeningbutton4
            // 
            this.Listeningbutton4.Enabled = false;
            this.Listeningbutton4.Location = new System.Drawing.Point(257, 244);
            this.Listeningbutton4.Name = "Listeningbutton4";
            this.Listeningbutton4.Size = new System.Drawing.Size(75, 23);
            this.Listeningbutton4.TabIndex = 21;
            this.Listeningbutton4.Text = "great";
            this.Listeningbutton4.UseVisualStyleBackColor = true;
            this.Listeningbutton4.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Listeningbutton3
            // 
            this.Listeningbutton3.Enabled = false;
            this.Listeningbutton3.Location = new System.Drawing.Point(349, 244);
            this.Listeningbutton3.Name = "Listeningbutton3";
            this.Listeningbutton3.Size = new System.Drawing.Size(75, 23);
            this.Listeningbutton3.TabIndex = 20;
            this.Listeningbutton3.Text = "good";
            this.Listeningbutton3.UseVisualStyleBackColor = true;
            this.Listeningbutton3.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Listeningbutton2
            // 
            this.Listeningbutton2.Enabled = false;
            this.Listeningbutton2.Location = new System.Drawing.Point(441, 244);
            this.Listeningbutton2.Name = "Listeningbutton2";
            this.Listeningbutton2.Size = new System.Drawing.Size(75, 23);
            this.Listeningbutton2.TabIndex = 19;
            this.Listeningbutton2.Text = "fair";
            this.Listeningbutton2.UseVisualStyleBackColor = true;
            this.Listeningbutton2.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Listeningbutton1
            // 
            this.Listeningbutton1.Enabled = false;
            this.Listeningbutton1.Location = new System.Drawing.Point(533, 244);
            this.Listeningbutton1.Name = "Listeningbutton1";
            this.Listeningbutton1.Size = new System.Drawing.Size(109, 23);
            this.Listeningbutton1.TabIndex = 18;
            this.Listeningbutton1.Text = "Need Improve";
            this.Listeningbutton1.UseVisualStyleBackColor = true;
            this.Listeningbutton1.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Speakingbutton5
            // 
            this.Speakingbutton5.Enabled = false;
            this.Speakingbutton5.Location = new System.Drawing.Point(165, 284);
            this.Speakingbutton5.Name = "Speakingbutton5";
            this.Speakingbutton5.Size = new System.Drawing.Size(75, 23);
            this.Speakingbutton5.TabIndex = 27;
            this.Speakingbutton5.Text = "Excellent";
            this.Speakingbutton5.UseVisualStyleBackColor = true;
            this.Speakingbutton5.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Speakingbutton4
            // 
            this.Speakingbutton4.Enabled = false;
            this.Speakingbutton4.Location = new System.Drawing.Point(257, 284);
            this.Speakingbutton4.Name = "Speakingbutton4";
            this.Speakingbutton4.Size = new System.Drawing.Size(75, 23);
            this.Speakingbutton4.TabIndex = 26;
            this.Speakingbutton4.Text = "great";
            this.Speakingbutton4.UseVisualStyleBackColor = true;
            this.Speakingbutton4.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Speakingbutton3
            // 
            this.Speakingbutton3.Enabled = false;
            this.Speakingbutton3.Location = new System.Drawing.Point(349, 284);
            this.Speakingbutton3.Name = "Speakingbutton3";
            this.Speakingbutton3.Size = new System.Drawing.Size(75, 23);
            this.Speakingbutton3.TabIndex = 25;
            this.Speakingbutton3.Text = "good";
            this.Speakingbutton3.UseVisualStyleBackColor = true;
            this.Speakingbutton3.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Speakingbutton2
            // 
            this.Speakingbutton2.Enabled = false;
            this.Speakingbutton2.Location = new System.Drawing.Point(441, 284);
            this.Speakingbutton2.Name = "Speakingbutton2";
            this.Speakingbutton2.Size = new System.Drawing.Size(75, 23);
            this.Speakingbutton2.TabIndex = 24;
            this.Speakingbutton2.Text = "fair";
            this.Speakingbutton2.UseVisualStyleBackColor = true;
            this.Speakingbutton2.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Speakingbutton1
            // 
            this.Speakingbutton1.Enabled = false;
            this.Speakingbutton1.Location = new System.Drawing.Point(533, 284);
            this.Speakingbutton1.Name = "Speakingbutton1";
            this.Speakingbutton1.Size = new System.Drawing.Size(109, 23);
            this.Speakingbutton1.TabIndex = 23;
            this.Speakingbutton1.Text = "Need Improve";
            this.Speakingbutton1.UseVisualStyleBackColor = true;
            this.Speakingbutton1.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Readingbutton5
            // 
            this.Readingbutton5.Enabled = false;
            this.Readingbutton5.Location = new System.Drawing.Point(165, 324);
            this.Readingbutton5.Name = "Readingbutton5";
            this.Readingbutton5.Size = new System.Drawing.Size(75, 23);
            this.Readingbutton5.TabIndex = 32;
            this.Readingbutton5.Text = "Excellent";
            this.Readingbutton5.UseVisualStyleBackColor = true;
            this.Readingbutton5.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Readingbutton4
            // 
            this.Readingbutton4.Enabled = false;
            this.Readingbutton4.Location = new System.Drawing.Point(257, 324);
            this.Readingbutton4.Name = "Readingbutton4";
            this.Readingbutton4.Size = new System.Drawing.Size(75, 23);
            this.Readingbutton4.TabIndex = 31;
            this.Readingbutton4.Text = "great";
            this.Readingbutton4.UseVisualStyleBackColor = true;
            this.Readingbutton4.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Readingbutton3
            // 
            this.Readingbutton3.Enabled = false;
            this.Readingbutton3.Location = new System.Drawing.Point(349, 324);
            this.Readingbutton3.Name = "Readingbutton3";
            this.Readingbutton3.Size = new System.Drawing.Size(75, 23);
            this.Readingbutton3.TabIndex = 30;
            this.Readingbutton3.Text = "good";
            this.Readingbutton3.UseVisualStyleBackColor = true;
            this.Readingbutton3.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Readingbutton2
            // 
            this.Readingbutton2.Enabled = false;
            this.Readingbutton2.Location = new System.Drawing.Point(441, 324);
            this.Readingbutton2.Name = "Readingbutton2";
            this.Readingbutton2.Size = new System.Drawing.Size(75, 23);
            this.Readingbutton2.TabIndex = 29;
            this.Readingbutton2.Text = "fair";
            this.Readingbutton2.UseVisualStyleBackColor = true;
            this.Readingbutton2.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Readingbutton1
            // 
            this.Readingbutton1.Enabled = false;
            this.Readingbutton1.Location = new System.Drawing.Point(533, 324);
            this.Readingbutton1.Name = "Readingbutton1";
            this.Readingbutton1.Size = new System.Drawing.Size(109, 23);
            this.Readingbutton1.TabIndex = 28;
            this.Readingbutton1.Text = "Need Improve";
            this.Readingbutton1.UseVisualStyleBackColor = true;
            this.Readingbutton1.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Grammarbutton5
            // 
            this.Grammarbutton5.Enabled = false;
            this.Grammarbutton5.Location = new System.Drawing.Point(165, 364);
            this.Grammarbutton5.Name = "Grammarbutton5";
            this.Grammarbutton5.Size = new System.Drawing.Size(75, 23);
            this.Grammarbutton5.TabIndex = 37;
            this.Grammarbutton5.Text = "Excellent";
            this.Grammarbutton5.UseVisualStyleBackColor = true;
            this.Grammarbutton5.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Grammarbutton4
            // 
            this.Grammarbutton4.Enabled = false;
            this.Grammarbutton4.Location = new System.Drawing.Point(257, 364);
            this.Grammarbutton4.Name = "Grammarbutton4";
            this.Grammarbutton4.Size = new System.Drawing.Size(75, 23);
            this.Grammarbutton4.TabIndex = 36;
            this.Grammarbutton4.Text = "great";
            this.Grammarbutton4.UseVisualStyleBackColor = true;
            this.Grammarbutton4.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Grammarbutton3
            // 
            this.Grammarbutton3.Enabled = false;
            this.Grammarbutton3.Location = new System.Drawing.Point(349, 364);
            this.Grammarbutton3.Name = "Grammarbutton3";
            this.Grammarbutton3.Size = new System.Drawing.Size(75, 23);
            this.Grammarbutton3.TabIndex = 35;
            this.Grammarbutton3.Text = "good";
            this.Grammarbutton3.UseVisualStyleBackColor = true;
            this.Grammarbutton3.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Grammarbutton2
            // 
            this.Grammarbutton2.Enabled = false;
            this.Grammarbutton2.Location = new System.Drawing.Point(441, 364);
            this.Grammarbutton2.Name = "Grammarbutton2";
            this.Grammarbutton2.Size = new System.Drawing.Size(75, 23);
            this.Grammarbutton2.TabIndex = 34;
            this.Grammarbutton2.Text = "fair";
            this.Grammarbutton2.UseVisualStyleBackColor = true;
            this.Grammarbutton2.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Grammarbutton1
            // 
            this.Grammarbutton1.Enabled = false;
            this.Grammarbutton1.Location = new System.Drawing.Point(533, 364);
            this.Grammarbutton1.Name = "Grammarbutton1";
            this.Grammarbutton1.Size = new System.Drawing.Size(109, 23);
            this.Grammarbutton1.TabIndex = 33;
            this.Grammarbutton1.Text = "Need Improve";
            this.Grammarbutton1.UseVisualStyleBackColor = true;
            this.Grammarbutton1.Click += new System.EventHandler(this.Grade_Button);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(48, 442);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 16);
            this.label14.TabIndex = 38;
            this.label14.Text = "Grade";
            // 
            // Gradebutton9
            // 
            this.Gradebutton9.Enabled = false;
            this.Gradebutton9.Location = new System.Drawing.Point(165, 439);
            this.Gradebutton9.Name = "Gradebutton9";
            this.Gradebutton9.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton9.TabIndex = 39;
            this.Gradebutton9.Text = "A+";
            this.Gradebutton9.UseVisualStyleBackColor = true;
            this.Gradebutton9.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton8
            // 
            this.Gradebutton8.Enabled = false;
            this.Gradebutton8.Location = new System.Drawing.Point(257, 439);
            this.Gradebutton8.Name = "Gradebutton8";
            this.Gradebutton8.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton8.TabIndex = 40;
            this.Gradebutton8.Text = "A";
            this.Gradebutton8.UseVisualStyleBackColor = true;
            this.Gradebutton8.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton7
            // 
            this.Gradebutton7.Enabled = false;
            this.Gradebutton7.Location = new System.Drawing.Point(349, 439);
            this.Gradebutton7.Name = "Gradebutton7";
            this.Gradebutton7.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton7.TabIndex = 41;
            this.Gradebutton7.Text = "A-";
            this.Gradebutton7.UseVisualStyleBackColor = true;
            this.Gradebutton7.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton6
            // 
            this.Gradebutton6.Enabled = false;
            this.Gradebutton6.Location = new System.Drawing.Point(165, 477);
            this.Gradebutton6.Name = "Gradebutton6";
            this.Gradebutton6.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton6.TabIndex = 42;
            this.Gradebutton6.Text = "B+";
            this.Gradebutton6.UseVisualStyleBackColor = true;
            this.Gradebutton6.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton5
            // 
            this.Gradebutton5.Enabled = false;
            this.Gradebutton5.Location = new System.Drawing.Point(257, 477);
            this.Gradebutton5.Name = "Gradebutton5";
            this.Gradebutton5.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton5.TabIndex = 43;
            this.Gradebutton5.Text = "B";
            this.Gradebutton5.UseVisualStyleBackColor = true;
            this.Gradebutton5.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton4
            // 
            this.Gradebutton4.Enabled = false;
            this.Gradebutton4.Location = new System.Drawing.Point(349, 477);
            this.Gradebutton4.Name = "Gradebutton4";
            this.Gradebutton4.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton4.TabIndex = 44;
            this.Gradebutton4.Text = "B-";
            this.Gradebutton4.UseVisualStyleBackColor = true;
            this.Gradebutton4.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton3
            // 
            this.Gradebutton3.Enabled = false;
            this.Gradebutton3.Location = new System.Drawing.Point(165, 517);
            this.Gradebutton3.Name = "Gradebutton3";
            this.Gradebutton3.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton3.TabIndex = 45;
            this.Gradebutton3.Text = "C+";
            this.Gradebutton3.UseVisualStyleBackColor = true;
            this.Gradebutton3.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton2
            // 
            this.Gradebutton2.Enabled = false;
            this.Gradebutton2.Location = new System.Drawing.Point(257, 517);
            this.Gradebutton2.Name = "Gradebutton2";
            this.Gradebutton2.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton2.TabIndex = 46;
            this.Gradebutton2.Text = "C";
            this.Gradebutton2.UseVisualStyleBackColor = true;
            this.Gradebutton2.Click += new System.EventHandler(this.Grade_Button);
            // 
            // Gradebutton1
            // 
            this.Gradebutton1.Enabled = false;
            this.Gradebutton1.Location = new System.Drawing.Point(349, 517);
            this.Gradebutton1.Name = "Gradebutton1";
            this.Gradebutton1.Size = new System.Drawing.Size(75, 23);
            this.Gradebutton1.TabIndex = 47;
            this.Gradebutton1.Text = "C-";
            this.Gradebutton1.UseVisualStyleBackColor = true;
            this.Gradebutton1.Click += new System.EventHandler(this.Grade_Button);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.BindingSource = this.bindingSource1;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Read,
            this.toolStripSeparator2,
            this.toolStripButton_load_Template,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(707, 25);
            this.bindingNavigator1.TabIndex = 49;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // toolStripButton_Read
            // 
            this.toolStripButton_Read.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Read.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Read.Image")));
            this.toolStripButton_Read.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Read.Name = "toolStripButton_Read";
            this.toolStripButton_Read.Size = new System.Drawing.Size(67, 22);
            this.toolStripButton_Read.Text = "Read Excel";
            this.toolStripButton_Read.Click += new System.EventHandler(this.Read_excel_click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_load_Template
            // 
            this.toolStripButton_load_Template.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_load_Template.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_load_Template.Image")));
            this.toolStripButton_load_Template.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_load_Template.Name = "toolStripButton_load_Template";
            this.toolStripButton_load_Template.Size = new System.Drawing.Size(88, 22);
            this.toolStripButton_load_Template.Text = "Load Template";
            this.toolStripButton_load_Template.Click += new System.EventHandler(this.Button_load_Template_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Save.Enabled = false;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(73, 22);
            this.toolStripButton_Save.Text = "Save Report";
            this.toolStripButton_Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            this.bindingNavigatorPositionItem.TextChanged += new System.EventHandler(this.BindingNavigatorPositionItem_TextChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
#if DEBUG
            // button_Test
            // 
            this.button_Test.Enabled = false;
            this.button_Test.Location = new System.Drawing.Point(508, 107);
            this.button_Test.Name = "button_Test";
            this.button_Test.Size = new System.Drawing.Size(134, 73);
            this.button_Test.TabIndex = 51;
            this.button_Test.Text = "Test";
            this.button_Test.UseVisualStyleBackColor = true;
            this.button_Test.Click += new System.EventHandler(this.Button_Test_Click);
            // 
#endif
            // button_Done
            // 
            this.button_Done.Enabled = false;
            this.button_Done.Location = new System.Drawing.Point(567, 39);
            this.button_Done.Name = "button_Done";
            this.button_Done.Size = new System.Drawing.Size(75, 23);
            this.button_Done.TabIndex = 48;
            this.button_Done.Text = "Next";
            this.button_Done.UseVisualStyleBackColor = true;
            this.button_Done.Click += new System.EventHandler(this.Done_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 589);
#if DEBUG
            this.Controls.Add(this.button_Test);
#endif
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.button_Done);
            this.Controls.Add(this.Gradebutton1);
            this.Controls.Add(this.Gradebutton2);
            this.Controls.Add(this.Gradebutton3);
            this.Controls.Add(this.Gradebutton4);
            this.Controls.Add(this.Gradebutton5);
            this.Controls.Add(this.Gradebutton6);
            this.Controls.Add(this.Gradebutton7);
            this.Controls.Add(this.Gradebutton8);
            this.Controls.Add(this.Gradebutton9);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Grammarbutton5);
            this.Controls.Add(this.Grammarbutton4);
            this.Controls.Add(this.Grammarbutton3);
            this.Controls.Add(this.Grammarbutton2);
            this.Controls.Add(this.Grammarbutton1);
            this.Controls.Add(this.Readingbutton5);
            this.Controls.Add(this.Readingbutton4);
            this.Controls.Add(this.Readingbutton3);
            this.Controls.Add(this.Readingbutton2);
            this.Controls.Add(this.Readingbutton1);
            this.Controls.Add(this.Speakingbutton5);
            this.Controls.Add(this.Speakingbutton4);
            this.Controls.Add(this.Speakingbutton3);
            this.Controls.Add(this.Speakingbutton2);
            this.Controls.Add(this.Speakingbutton1);
            this.Controls.Add(this.Listeningbutton5);
            this.Controls.Add(this.Listeningbutton4);
            this.Controls.Add(this.Listeningbutton3);
            this.Controls.Add(this.Listeningbutton2);
            this.Controls.Add(this.Listeningbutton1);
            this.Controls.Add(this.Phonicsbutton1);
            this.Controls.Add(this.Phonicsbutton2);
            this.Controls.Add(this.Phonicsbutton3);
            this.Controls.Add(this.Phonicsbutton4);
            this.Controls.Add(this.Phonicsbutton5);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label_StuRoom);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label_StuName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_StuEnGroup);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_StuEnName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft JhengHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_StuEnName;
        private System.Windows.Forms.Label label_StuEnGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_StuName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_StuRoom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button Phonicsbutton5;
        private System.Windows.Forms.Button Phonicsbutton4;
        private System.Windows.Forms.Button Phonicsbutton3;
        private System.Windows.Forms.Button Phonicsbutton2;
        private System.Windows.Forms.Button Phonicsbutton1;
        private System.Windows.Forms.Button Listeningbutton5;
        private System.Windows.Forms.Button Listeningbutton4;
        private System.Windows.Forms.Button Listeningbutton3;
        private System.Windows.Forms.Button Listeningbutton2;
        private System.Windows.Forms.Button Listeningbutton1;
        private System.Windows.Forms.Button Speakingbutton5;
        private System.Windows.Forms.Button Speakingbutton4;
        private System.Windows.Forms.Button Speakingbutton3;
        private System.Windows.Forms.Button Speakingbutton2;
        private System.Windows.Forms.Button Speakingbutton1;
        private System.Windows.Forms.Button Readingbutton5;
        private System.Windows.Forms.Button Readingbutton4;
        private System.Windows.Forms.Button Readingbutton3;
        private System.Windows.Forms.Button Readingbutton2;
        private System.Windows.Forms.Button Readingbutton1;
        private System.Windows.Forms.Button Grammarbutton5;
        private System.Windows.Forms.Button Grammarbutton4;
        private System.Windows.Forms.Button Grammarbutton3;
        private System.Windows.Forms.Button Grammarbutton2;
        private System.Windows.Forms.Button Grammarbutton1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button Gradebutton9;
        private System.Windows.Forms.Button Gradebutton8;
        private System.Windows.Forms.Button Gradebutton7;
        private System.Windows.Forms.Button Gradebutton6;
        private System.Windows.Forms.Button Gradebutton5;
        private System.Windows.Forms.Button Gradebutton4;
        private System.Windows.Forms.Button Gradebutton3;
        private System.Windows.Forms.Button Gradebutton2;
        private System.Windows.Forms.Button Gradebutton1;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.BindingSource bindingSource1;
#if DEBUG
        private System.Windows.Forms.Button button_Test;
#endif
        private System.Windows.Forms.ToolStripButton toolStripButton_Read;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_load_Template;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Button button_Done;
    }

}

